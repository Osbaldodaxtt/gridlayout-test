import { Component, OnInit } from '@angular/core';
import { Tilestructure } from '../grid-structure';
import { User } from '../user-structure';


@Component({
  selector: 'app-gridview',
  templateUrl: './gridview.component.html',
  styleUrls: ['./gridview.component.scss']
})
export class GridviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  tiles: Tilestructure[] = [
    {cols: 1, rows: 5, name:'Sidenav'},
    {cols: 1, rows: 5, name:'Main information'},
  ];
}
