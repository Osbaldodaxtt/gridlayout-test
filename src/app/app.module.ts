import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GridviewComponent } from './gridview/gridview.component';
import { MaterialModule } from './material/material.module';
import { DummyComponent } from './dummy/dummy.component';
import { FlexlayoutComponent } from './flexlayout/flexlayout.component';

@NgModule({
  declarations: [
    AppComponent,
    GridviewComponent,
    DummyComponent,
    FlexlayoutComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
