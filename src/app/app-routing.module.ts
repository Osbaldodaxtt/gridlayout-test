import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GridviewComponent } from './gridview/gridview.component';
import { FlexlayoutComponent } from './flexlayout/flexlayout.component';
import { DummyComponent } from './dummy/dummy.component';

const routes: Routes = [
  {path: "grid", component: GridviewComponent},
  {path: "flex", component: FlexlayoutComponent},
  {path: "", redirectTo: 'dummy', pathMatch: 'full'},
  {path: "dummy", component: DummyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
