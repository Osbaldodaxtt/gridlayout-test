import { Component, OnInit } from '@angular/core';
import { User } from '../user-structure';

@Component({
  selector: 'app-flexlayout',
  templateUrl: './flexlayout.component.html',
  styleUrls: ['./flexlayout.component.scss']
})
export class FlexlayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  users: User[] = [
    {name: 'Sergio ', lastname: 'Dominguez', age: 25, address: 'Begonias 221-A', phone: '+529613750209'},
    {name: 'Sergio ', lastname: 'Dominguez', age: 25, address: 'Begonias 221-A', phone: '+529613750209'},
    {name: 'Sergio ', lastname: 'Dominguez', age: 25, address: 'Begonias 221-A', phone: '+529613750209'},
    {name: 'Sergio ', lastname: 'Dominguez', age: 25, address: 'Begonias 221-A', phone: '+529613750209'},
    {name: 'Sergio ', lastname: 'Dominguez', age: 25, address: 'Begonias 221-A', phone: '+529613750209'},
    {name: 'Sergio ', lastname: 'Dominguez', age: 25, address: 'Begonias 221-A', phone: '+529613750209'}
  ];
}
