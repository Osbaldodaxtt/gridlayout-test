import { Component } from '@angular/core';

export interface Card{
  value: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'grid-examples';
  cards: Card[] = [
    {value: 'Card for example'},
    {value: 'This is another example'},
    {value: 'This information is just for testing'}
  ];
}
