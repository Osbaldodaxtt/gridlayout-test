export interface Tilestructure {
    name: string;
    cols: number;
    rows: number;
}